FROM Python:latest

COPY runtests_tinder_server.py .
COPY tinder_server.py .
CMD ["python", "./tinder_server.py"]